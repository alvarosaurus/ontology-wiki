# Create a standalone wiki.
# A SQL-Lite database is mounted on the host.
# Edit confguration in the file config.ini

.DEFAULT_GOAL := build

# Read config file
include config.ini

# Build semantic MediaWiki
# see: https://www.mediawiki.org/wiki/Manual:Config_script
build:rm
# build and start the (unconfigured) containers
	$(export_env) && \
	docker-compose -f docker-compose-install.yml up -d

install:|install_wiki semantics
	echo "wiki was installed. See http://localhost"

install_wiki:
# Install the database, wiki and server configurations
# run the MediaWiki installation script
	docker exec -ti ${SMW_CONTAINER_NAME} script -q -c "\
	php /var/www/wiki/maintenance/install.php \
	--pass ${PASS} \
	--email ${EMAIL} \
	--scriptpath ${SCRIPTPATH} \
	--lang ${LANG} \
	--dbtype ${DBTYPE} \
	--dbserver ${DB_CONTAINER_NAME} \
	--dbname ${DBNAME} \
	--installdbuser ${INSTALLDBUSER} \
	--installdbpass ${INSTALLDBPASS} \
	--dbuser ${DBUSER} \
	--dbpass ${DBPASS} \
	--with-extensions \
	${NAME} ${ADMIN}"
# create the Sysop for the wiki
	docker exec -ti ${SMW_CONTAINER_NAME} script -q -c "\
	php /var/www/wiki/maintenance/createAndPromote.php --sysop --bureaucrat --force Sysop ${PASS}"
# set the article path, allow using icons from wikipedia, enable semantics, enable string parser functions
	docker exec -ti ${SMW_CONTAINER_NAME} script -q -c "\
	sed -i 's|$$wgScriptPath = \"/html\";|$$wgScriptPath = \"/wiki\";|g' /var/www/wiki/LocalSettings.php; \
	sed -i 's|\# Add more configuration options below.|\
	\# Add more configuration options below.\n\
	enableSemantics\( \"localhost\" \);\n\
	\$$wgAllowExternalImagesFrom = \"https://upload.wikimedia.org/\";\n\
	\$$wgPFEnableStringFunctions = true;\
	|g' /var/www/wiki/LocalSettings.php;"

semantics:
# run composer update (creates database tables)
	docker exec -ti ${SMW_CONTAINER_NAME} script -q -c "\
	cd /var/www/wiki && \
	php ./composer.phar update --no-dev && \
	php maintenance/update.php --quick"
# Update Semantic MediaWiki
	docker exec -ti ${SMW_CONTAINER_NAME} script -q -c "\
	cd /var/www/wiki && \
	php extensions/SemanticMediaWiki/maintenance/rebuildData.php -ftpv && \
	php extensions/SemanticMediaWiki/maintenance/rebuildData.php -v && \
	php maintenance/runJobs.php"

save:
# dump the database and copy it to the data container
	docker exec ${DB_CONTAINER_NAME}  script -q -c "/usr/bin/mysqldump --complete-insert -u${DBUSER} -p${DBPASS} ${DBNAME} > /tmp/dump.sql"
	docker cp ${DB_CONTAINER_NAME}:/tmp/dump.sql /tmp
	docker cp /tmp/dump.sql ${DATA_CONTAINER_NAME}:/
# save the images, including the container layer
	docker commit ${DATA_CONTAINER_NAME} ${DATA_CONTAINER_NAME}
	docker commit ${SMW_CONTAINER_NAME} ${SMW_CONTAINER_NAME}
	docker commit ${DB_CONTAINER_NAME} ${DB_CONTAINER_NAME}

restore:
# re-create the database
	docker exec -ti ${DB_CONTAINER_NAME} script -q -c "echo \"drop database if exists ${DBNAME};\" > /tmp/query.sql"
	docker exec -ti ${DB_CONTAINER_NAME} script -q -c "mysql -u${DBUSER} -p${DBPASS} ${DBNAME} < /tmp/query.sql"
	docker exec -ti ${DB_CONTAINER_NAME} script -q -c "mysqladmin -u${DBUSER} -p${DBPASS} create ${DBNAME}"
# import database dump
	docker cp ${DATA_CONTAINER_NAME}:/dump.sql /tmp/
	docker cp /tmp/dump.sql ${DATA_CONTAINER_NAME}:/tmp
	docker exec -ti ${DB_CONTAINER_NAME} script -q -c "mysql -u${DBUSER} -p${DBPASS} ${DBNAME} < /tmp/dump.sql"

# Stop and remove
rm:
	-docker stop ${SMW_CONTAINER_NAME}
	-docker rm ${SMW_CONTAINER_NAME}
	-docker stop ${DB_CONTAINER_NAME}
	-docker rm ${DB_CONTAINER_NAME}

define export_env
	export SMW_CONTAINER_NAME=${SMW_CONTAINER_NAME} && \
	export DB_CONTAINER_NAME=${DB_CONTAINER_NAME} && \
	export INSTALLDBPASS=${INSTALLDBPASS} && \
	export MOUNT=${MOUNT}
endef

run:
	$(up)
