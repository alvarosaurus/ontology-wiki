# Settings for MediaWiki and database
# use 'make configure' to generate this file

### Docker settings ###

# Name of the Docker container with the wiki and the webserver
SMW_CONTAINER_NAME=@@SMW_CONTAINER_NAME@@
# Name of the Docker container with the database engine
DB_CONTAINER_NAME=@@DB_CONTAINER_NAME@@
# Name of the Docker container with the data for restoring the wiki
DATA_CONTAINER_NAME=basic-data

#### MediaWiki and database settings ###
# see: https://www.mediawiki.org/wiki/Manual:Install.php

# The password for the wiki administrator
PASS=@@PASS@@
# The relative path of the wiki in the web server (/wiki)
SCRIPTPATH=/html
# The language to use (en)
LANG=en
# The type of database (mysql)
DBTYPE=mysql
# The database name (my_wiki)
DBNAME=basic_wiki
# The user to use for installing (root)
INSTALLDBUSER=root
# The password for the DB user to install as
INSTALLDBPASS=@@INSTALLDBPASS@@
# The DB user to use for this script.
# This value appears in LocalSettings.php for $wgDBuser.
# If --installdbuser and --installdbpass are given,
# this value will be used to create a new account
DBUSER=root
# The password to use for this script.
# This value appears in LocalSettings.php for $wgDBpassword.
# If --installdbuser and --installdbpass are given,
# this value will be used to create a new account
DBPASS=@@INSTALLDBPASS@@
# The name of the wiki (MediaWiki)
NAME=@@NAME@@
# The username of the wiki administrator
ADMIN=Sysop