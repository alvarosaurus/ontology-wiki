Create a wiki with Semantic MediaWiki extension, for testing purposes.

This container is useful for running unit tests against a wiki (e.g. with selenium or for testing the API).

# Using the preinstalled images
1. Clone the repository.
2. Start the wiki. The preinstalled images will be downloaded from docker hub `docker-compose up -d`
3. Restore the database with the pre-installed wiki `make restore`

# Building from source
If you wish to build from source, proceed as below. Once the wiki is running and you have made changes that you need to save, you can save the wiki and the data into new images (see below).

## The following docker images will be created:
### smw
Extends mediawiki:1.31.1 official image
* MediaWiki 1.31 with Semantic MediaWiki extensions
* Apache 2.4.25 and "short URLs"
* PHP 7.2.17
* Debian GNU/Linux 9

### db
MariaDB official image, provides
* MariaDB 10.4
* Ubuntu 18.04.2 LTS

### data
Data is stored on the host in a volume managed by Docker. Once the wiki is running, you can save your data to a wiki image for later reuse (optional).
* Debian stretch
* A database dump of the pre-installed wiki

## Installing the wiki
Use `configure`, `make`, `make install` to setup the containers.

Create the wiki configuration
```
./configure
```
The defaults should be alright for testing. Settings are stored in `config.ini`. You can edit the file `config.ini` for additional settings afterwards.

Build the docker images
```
make
```

Install the wiki and the database
```
make install
```
You can now open the wiki at http://localhost
The administrator login pre-configured in config.ini is login "Sysop", '''password is stored in cleartext in the config.ini file'''.

## Transient data
Data is stored in a volume managed by Docker.
The data will be reset every time the wiki is rebuilt. 

To keep the data, so `make save`. This will create 3 images:
* basic-wiki, containing the installed wiki
* basic-db, containing the database engine
* basic-data, containing the database data.


## Starting and stopping the wiki
(Re)start the wiki
`docker-compose up -d`
Note that if you build your own images, you might need to edit the docker-compose.ymk file, otherwise the images will be downloaded from Docker hub by default.

Stop the wiki
`docker-compose down`

Stop the wiki and remove the container
```
make rm
```
